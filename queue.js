let collection = [];

//Write the queue functions below.
//NOTE: DO NOT USE ANY ARRAY METHODS. YOU CAN USE .length property

// List of array methods that you SHOULD NOT use:
// concat(), copyWithin(), entries(), every(), fill(), filter(), find(), findIndex(), from(), includes(), indexOf(), isArray(), join(), keys(), lastIndexOf(), map(), pop(), push(), reduce(), reduceRight(), reverse(), shift(), slice(), some(), sort(), splice(), toString(), unshift(), valueOf()

//Exception: forEach()
//NOTE: USE return KEYWORD AND NOT console.log() FOR RETURNING VALUES

function print(){

	// Return the value of the array
	return collection;

}


function enqueue(element){

	// Add element at the end of the queue
	// Return the value of the array
	// With an array method: .push()
	// Note: Do not use array methods except forEach()

	collection[collection.length]=element;
	return collection;

}


function dequeue(){

	// Remove the first element of the collection array 
	// Return the updated/manipulated collection array
	// With array method: shift()
	// Note: Do not use array methods except forEach()

	let newArray = [];

	for(i=0;i<collection.length-1;i++){
		newArray[i]=collection[i+1];
	}

	collection = newArray;
	return collection;

	/*

	Alternative

	let newArray = []
	let i=0;
	let shouldSkip=true;
	
	collection.forEach(item=>{
		if(item!==null){
			if(shouldSkip){
				shouldSkip=false;
			} else {
				newArray[i]=item;
				i++;
				
			}
		}
	})

	collection = newArray;
	return collection;

	*/
	

}

function front(){

	//Return the first item in the collection array

	return collection[0];

	/*
	
	Alternative

	let firstItem = "";
	let shouldSkip = false;

	collection.forEach(item=>{

		if(shouldSkip){
			return;
		} 
		if(item!==null){
			firstItem=item;
			shouldSkip=true;
		}

	})

	return firstItem;

	*/

}

function size(){

	//Return the current number of items in the array

	return collection.length;

	/*

	Alternative
	
	let count = 0;

	collection.forEach(item=>{
		if(item!==null){count++}
	})
	return count;

	*/

}

function isEmpty(){

	
	//Check if the array is empty or not and return a boolean.
	
	if(collection.length===0){return true;}else{return false}

	/*

	Alternative 

	let empty = true;

	collection.forEach(item=>{
		if (item!==null){
			empty=false;
		}
	})
	return empty;
	
	*/
}



module.exports = {

print,
enqueue,
dequeue,
front,
size,
isEmpty

};